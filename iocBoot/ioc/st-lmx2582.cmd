< envPaths

errlogInit(20000)

dbLoadDatabase("$(IOCAPP)/dbd/iocApp.dbd")
iocApp_registerRecordDeviceDriver(pdbbase)


epicsEnvSet("ETHMOD_IP",           "172.30.150.48")

#epicsEnvSet("EPICS_CA_ADDR_LIST",          "127.0.0.1")
epicsEnvSet("EPICS_CA_ADDR_LIST",           "$(ETHMOD_IP)")
epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST",      "NO")

# PATH: we need caput and caRepeater
epicsEnvSet("PATH",                         "$(PATH):$(EPICS_BASE)/bin/linux-x86_64")
# EPICS_CA_MAX_ARRAY_BYTES: 10 MB max CA request
# epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
# EPICS_DB_INCLUDE_PATH: list all module db folders
epicsEnvSet("EPICS_DB_INCLUDE_PATH",        "$(TOP)/db:$(ETHMOD)/db:$(ETHMODAPP)/db")

# Prefix for all records
epicsEnvSet("PREFIX",               "ETHMOD:")
epicsEnvSet("SPI_LMX2582_PORT",     "AK_SPI_LMX2582")
epicsEnvSet("SPI_IP_PORT",          "AK_SPI_COMM")

###
# SPI
###
# Create the asyn port to talk to the AK-NORD server on command port 1002.
# port 1
# drvAsynIPPortConfigure($(SPI_IP_PORT),"$(ETHMOD_IP):1002")
# port 2
drvAsynIPPortConfigure($(SPI_IP_PORT),"$(ETHMOD_IP):1003")

# asynSetTraceIOMask($(SPI_IP_PORT),0,255)
# asynSetTraceMask($(SPI_IP_PORT),0,255)

# Set the terminators
#asynOctetSetOutputEos($(SPI_IP_PORT), 0, "\003")
#asynOctetSetInputEos($(SPI_IP_PORT), 0,  "\003")

AKSPILMX2582Configure($(SPI_LMX2582_PORT), $(SPI_IP_PORT), 0, 0)
dbLoadRecords("AKSPI_LMX2582.db",       "P=$(PREFIX),R=SPI:LMX2582:,PORT=$(SPI_LMX2582_PORT),IP_PORT=$(SPI_IP_PORT),ADDR=0,TIMEOUT=1")
# asynSetTraceIOMask($(SPI_LMX2582_PORT),0,255)
# asynSetTraceMask($(SPI_LMX2582_PORT),0,255)


iocInit()

# dbpf ETHMOD:SPI:LMX2582:WriteAll 1

# dbpf ETHMOD:SPI:LMX2582:ReadAll 1
